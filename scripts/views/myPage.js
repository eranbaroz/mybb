﻿/// <reference path="../lib/backbone.js" />
/// <reference path="../lib/underscore.js" />
/// <reference path="../lib/jquery-1.8.3.js" />

(function () {
    var myPage = getModule('myPage');

    myPage.MyModel = Backbone.Model.extend({
        idAttribute: 'Id',
        defaults: {
            Id: -1,
            Name: '',
            Category: '',
            Price: ''
        },
        url: '/TestAPI/API/product/'
    });

    myPage.MyCollection = Backbone.Collection.extend({
        model: myPage.MyModel,
        url: '/TestAPI/API/product'
    });


    myPage.MyView = Backbone.View.extend({
        tagName: 'tbody',
        className: 'mydata',
        initialize: function () {
            _.bindAll(this, 'render', 'draw');
            this.listenTo(this.collection, 'sync', function () {
                this.draw();
            });
        },
        template: 'hello-world',
        render: function () {
            this.collection.each(function (m) {
                this.$el.append(_.template(jst.templates[this.template])(m.toJSON()));
            }, this);

            return this;
        },
        draw: function () {
            $('.prods').append(this.render().$el.html());
        }
    });

    myPage.load = function () {
        $('.main-container').html(jst.templates['mypage']);
        var mymodel = new myPage.MyModel();
        var mycollection = new myPage.MyCollection();
        var myview = new myPage.MyView({collection:mycollection});
        mycollection.fetch();
    }

})();