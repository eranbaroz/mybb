﻿/// <reference path="../lib/jquery-1.9.1.js" />
/// <reference path="../scripts/lib/backbone.js" />
var jst = jst || {};
(function () {
    if (jst.isLoaded) {
        return;
    }

    jst.templates = {};

    $.get('templates/GetTemplates.ashx',
        function getTemplatesSuccess(data) {
            $(data).filter('script').each(function (i, el) {
                //template's id
                var id = $(el).attr('id');

                //check for existing id
                if (typeof jst.templates[id] !== 'undefined') {
                    var err = 'Err - DUPLICATE_TEMPLATE_ID: ' + id;
                    apploge(err);
                    jst.isLoaded = false;
                    jst.error = err;
                    //return false;
                } else {

                    //template's content
                    var html = $(el).html();
                    jst.templates[id] = html;

                    jst.isLoaded = true;
                }
            });
            Backbone.Events.trigger('jst-done', { isLoaded: jst.isLoaded, error: jst.error });
        }
    );

    //var test = $('<test>').load('templates/templates.html', function (data, result, jqXhr) {
    //    if (result === 'success') {

    //    }
    //});

}());