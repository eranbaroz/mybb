﻿<%@ WebHandler Language="C#" Class="GetTemplates" %>

using System;
using System.Web;

public class GetTemplates : IHttpHandler {
    
    public void ProcessRequest (HttpContext context) {
        
        System.Collections.Generic.List<string> list = new System.Collections.Generic.List<string>();
        System.Text.StringBuilder templates = new System.Text.StringBuilder();

        foreach (string file in System.IO.Directory.GetFiles(context.Server.MapPath(""), "*.html")){
            System.IO.StreamReader sr = null;
            try
            {
                sr = new System.IO.StreamReader(file);
                templates.Append(sr.ReadToEnd());

            }
            catch (Exception ex)
            {
                templates.AppendLine(string.Format("Ooops Error occured: {0}", ex.Message));
            }
            finally {
                if (sr != null)
                {
                    sr.Close();
                }
            }
        }

        //context.Response.Write(new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(list));
        context.Response.ContentType = "text/html";
        context.Response.Write(templates.ToString());
    }
 
    public bool IsReusable {
        get {
            return false;
        }
    }

}