﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="doc.aspx.cs" Inherits="templates_doc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>Templates</h1>
        <h2>Under The Hood:</h2>
        <fieldset><legend>.html files</legend>
            each file is named after a page (a convention - not a must). Each html file contains script templates blocks.
        </fieldset>
        <fieldset><legend>GetTemplates.ashx</legend>
            server side code that accessed by ajax and returns a string of all the text in the html files.
        </fieldset>
        <fieldset><legend>Router.js</legend>
            The main kick start for the UI parts of the application. each route event loads the mail html structure with the function loadPage
        </fieldset>
        <fieldset><legend>JST (Javascript Templates)</legend> 
            a live object that hold the templates code. 
            <ul>
                <li>jst.isLoaded is a loading flag (isLoaded)</li>
                <li>jst.error is an error string in case of error</li>
                <li>jst.templates is an object containing all the templates by their id attribute (e.g. jst.templates['hello-world']</li>
            </ul>
        </fieldset>
        <fieldset><legend>app.js</legend>
            hold global functions for common tasks
            <ul>
                <li>apploge - output a message meant for errors to the console log in the form of '*** E: ' + msg</li>
                <li>applogi - output a message meant for info and debug to the console log in the form of '*** I: ' + msg</li>
            </ul>
        </fieldset>
    </div>
    </form>
</body>
</html>
