﻿(function () {
    if (typeof app === 'undefined') {
        apploge('ERROR!!! App not started');
        return false;
    }

    var Router = Backbone.Router.extend({

        routes: {
            "": "home",
            "mypage": "mypage",
            "help": "help",    // #help
            "search/:query": "search",  // #search/kiwis
            "search/:query/p:page": "search"   // #search/kiwis/p7
        },
        loadPage: function (pageName) {
            $('.main-container').html(jst.templates[pageName]);
        },
        home: function () {
            this.loadPage('home');
            applogi('route home()');
        },
        mypage: function () {
            app.myPage.load();
            applogi('route mypage()');
        },
        help: function () {
            applogi('route help()');
        },

        search: function (query, page) {
            applogi('route search()');
        }

    });

    app.router = new Router();
    //don't load anything and don't start hsitory listener before all templates are loaded
    Backbone.Events.on('jst-done', function (jst) {
        if (jst.isLoaded) {
            applogi('jst-done');
            //wait for the dom until it's ready. this should be the one and only listener for on document ready
            $(document).ready(function () {
                Backbone.history.start();
            });
        }
        else {
            apploge('templates load error');
        }
    });
    
}());
