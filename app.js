﻿var app = app || {};

var getModule = function (moduleName) {
    app[moduleName] = app[moduleName] || {};
    return app[moduleName];
};

var applogi = function (msg) {
    console.log('*** I: ' + msg);
};

var apploge = function (msg) {
    console.log('*** E: ' + msg);
};