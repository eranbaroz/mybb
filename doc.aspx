﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="doc.aspx.cs" Inherits="templates_doc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>Root folder</h1>
        <h2>Under The Hood:</h2>
        <fieldset><legend>app.js</legend>
            hold global functions for common tasks
            <ul>
                <li>Console Log functions: use these function instead of calling direct to console.log. give the ability to silence all the prints in one central place.
                    <ul>
                        <li>apploge - output a message meant for errors to the console log in the form of '*** E: ' + msg</li>
                        <li>applogi - output a message meant for info and debug to the console log in the form of '*** I: ' + msg</li>
                    </ul>
                </li>
            </ul>
        </fieldset>
        <fieldset><legend>router.js</legend>
            <ul>
                <li>holds the main navigation defintion for the site.</li>
                <li>router.loadPage - loads the main structure of each page</li>
            </ul>
        </fieldset>
    </div>
    </form>
</body>
</html>
